CREATE DATABASE Formative14;

use Formative14;

CREATE TABLE Brand(
id INT auto_increment,
name varchar(50),
primary key(id)
);

CREATE TABLE product(
id INT auto_increment,
artnumber varchar(50),
name varchar(50),
description varchar(150),
brandId int,
primary key(id),
foreign key(brandId) REFERENCES Brand(id)
);

INSERT INTO brand (name)
VALUES
("Xiaomi"),
("Samsung"),
("Apple");



INSERT INTO product(artnumber, name, description, brandId)
VALUES
("X-0001", "Xiaomi Mi 11", "128GB", 1),
("S-0001", "Samsung Galaxy S10","256GB", 2),
("A-0001", "Iphone 13 Promax", "256GB", 3);
