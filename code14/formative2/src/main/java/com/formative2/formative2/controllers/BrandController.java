package com.formative2.formative2.controllers;

import com.formative2.formative2.models.entities.Brand;
import com.formative2.formative2.models.repos.BrandRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
public class BrandController {

    @Autowired
    private BrandRepository repo;
    
    @GetMapping("/")
    public List<Brand> getAllBrand() {
        return repo.findAll();
    }

    @GetMapping("/brand/{id}")
    public Brand getBrandById(@PathVariable(value = "id") int id){
        return repo.findById(id);
    }
    
}
