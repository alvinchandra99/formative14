package com.formative2.formative2.models.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;
    private String artnumber;

    private String description;

    private int brandId;


    public Product() {
    }

    public Product(int id, String name, String artnumber, String description, int brandId) {
        this.id = id;
        this.name = name;
        this.artnumber = artnumber;
        this.description = description;
        this.brandId = brandId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtnumber() {
        return artnumber;
    }

    public void setArtnumber(String artnumber) {
        this.artnumber = artnumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    

   
    

    
    
}
