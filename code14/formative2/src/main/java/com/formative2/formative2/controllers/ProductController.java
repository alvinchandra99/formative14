package com.formative2.formative2.controllers;

import java.util.List;

import com.formative2.formative2.models.entities.Product;
import com.formative2.formative2.models.repos.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@RestController
public class ProductController {

    @Autowired
    private ProductRepository repo;
    
    @GetMapping("/product")
    public List<Product> getAllProduct() {
        return repo.findAll();
    }

    @GetMapping("/product/{id}")
    public Product getProductById(@PathVariable(value = "id") int id){
        return repo.findById(id);
    }
    
}
