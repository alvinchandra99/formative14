package com.formative2.formative2.models.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.formative2.formative2.models.entities.Brand;
import java.util.List;

@Repository
public interface BrandRepository extends CrudRepository <Brand, Integer>{

    Brand findById(int id);
    List<Brand> findAll();
    void deleteById(int id);
    
}
